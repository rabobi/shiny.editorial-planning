# Shiny App for the Editorial Planning Tool
A dockarized app created using R Shiny for the Editorial Planning Tool

## Getting Started
**This app is still a POC**

### Requirements

* Docker

**For Windows:**

```
cd C:\git\shiny.editorial-planning # local path of repository

docker build -t editorial-planning .

docker run -it -p 3838:3838 editorial-planning
```


## Built With
* R Studio
* ShinyDashboard


## ShinyProxy

 
```
cd C:\git\shiny.editorial-planning\ShinyProxy

java -jar shinyproxy-2.2.1.jar  
```

Default Users:
admin
user

password:
l2c