FROM openanalytics/r-base

MAINTAINER Gerru "gerru.kloppers@bonial.com"

# system libraries of general use
RUN apt-get update && apt-get install -y \
    sudo \
    pandoc \
    pandoc-citeproc \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    libssl-dev \
    libssh2-1-dev \
    libssl1.0.0

# system library dependency for the RPostgres library
RUN apt-get update && apt-get install -y \
    libpq-dev

# basic shiny functionality
RUN R -e "install.packages(c('shiny', 'DT', 'shinydashboard'), repos='https://cloud.r-project.org/')"

# install dependencies
RUN R -e "install.packages(c('dplyr', 'DBI', 'RPostgres'), repos='https://cloud.r-project.org/')"

# copy the app to the image
RUN mkdir /root/et
COPY et /root/et

COPY Rprofile.site /usr/lib/R/etc/

EXPOSE 3838

CMD ["R", "-e", "shiny::runApp('/root/et/')"]
